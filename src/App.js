import React, {useState, createContext} from 'react';
import './App.css';
import Navbar from "./components/NavBar";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Home from './pages';
import Course from './pages/course';
import logo from './logo.svg';
export const LoginContext = createContext();

function App() {
    // eslint-disable-next-line no-unused-vars
    const [loggedIn, setLoggedIn] = useState(true);

    return (
        <LoginContext.Provider value={loggedIn}>
                    <div>
                        <header className="App-header">
                            <img src={logo} className="App-logo" alt="logo"/>
                            <span className="App-title">Angular 15 Course</span>
                            <Router>
                                <Navbar/>
                                <Routes>
                                    <Route path='/' element={<Home/>}/>
                                    <Route path='/course' element={<Course/>}/>
                                </Routes>
                            </Router>
                        </header>
                    </div>
        </LoginContext.Provider>
    );
}

export default App;
