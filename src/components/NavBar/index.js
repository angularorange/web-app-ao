import React, {useContext, useEffect} from 'react';
import {LoginContext} from '../../App';

import {
    Nav,
    NavLink,
    Bars,
    NavMenu
} from './NavbarElements';

const Navbar = () => {
    const loggedIn = useContext(LoginContext);
    useEffect(()=> console.log(loggedIn));

    return (
        <>
            <Nav>
                <Bars />
                <NavMenu>
                    <NavLink to= '/course'   state={{ category: "Introduction" }}>
                        Introduction
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Data Binding" }}>
                        Data Binding
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Dependency Injection" }}>
                        Dependency Injection
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Observables" }}>
                        Observables
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Communication" }}>
                        Communication
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Template Driven Forms" }}>
                        Template Driven Forms
                    </NavLink>
                    <NavLink to= '/course'   state={{ category: "Reactive Forms" }}>
                        Reactive Forms
                    </NavLink>
                </NavMenu>


            </Nav>
        </>
    );
};

export default Navbar;
