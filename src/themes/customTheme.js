
import {createTheme} from 'styled-components'

export const theme = createTheme() ({
    palette: {
        primary: {
            main: '#ff3108'
        }
    }
});
