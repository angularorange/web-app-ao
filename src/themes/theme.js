export const base = {
  breakpoints: ['768px'],
    space: ['0px', '2px', '4px', '8px', '16px', '32px', '64px'],
    fonts: {
    heading: 'Inter, system-ui, sans-serif',
        body: 'Inter, system-ui, sans-serif',
},
fontSizes: ['12px', '14px', '16px', '20px', '24px']
}
export const light = {
    primary: '#4851f4',
    secondary: '#f4eb48',
    background: '#ffffff',
    nav: '#f8f8f8',
    border: '#deebf1',
    text: '#df2e2e'
}
export const dark = {
    primary: '#c41b96',
    secondary: '#cf49f5',
    background: '#1f2023',
    nav: '#27282b',
    border: '#303236',
    text: '#f8f8f8'
}
