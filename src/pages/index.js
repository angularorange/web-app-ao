import React from 'react';

const Home = () => {
    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'top',
                alignItems: 'center',
                height: '100vh'
            }}
        >
            <h2>Welcome to Angular 15 Complete Basic Course</h2>
            <br></br>
            <span className="Span-text">This course is a full basic Angular 15 course provided by <a href="https://angularorange.io">Angular Orange, LLC.</a> Angular Orange provides a complete set of Angular Framework training and specializes in AWS Serverless Technologies and Industry Solutions.
                Although this site is driving a complete basic Angular 15 class, its purpose is to demonstrate the power of a React 18 Component and how it can drive videos
                for training using simple react functional components, hooks and the AWS serverless stack. The React Component uses an LayoutEffect hook to retrieve JSON off AWS Serverless technology (e.g. S3, Cloud Watch, IOA) to drive the
                videos in the training component. We call this the React Course Component). The Course Component also uses props during navigation
                to share active course category and dynamic retrieval of JSON off AWS using axios client</span>

            <h2>Note</h2>
            <span className="Span-text">This React Component is presently only being used to drive video content for a new Angular 15 / 16 training videos provided by <a href="https://angularorange.io">Angular Orange, LLC.</a>.
                The Course Component is being used only to exhibit the basic behavior and is not meant to be a fully best practice app. However it presently being extended with white labeling functionality and other features and hooks to use in practical applications.
            </span>
            <br></br>
            <span className="Span-text">
                 <a href="https://gitlab.com/angularorange/web-app-ao">Download the React 18 Course code from Gitlab.</a>
            </span>
        </div>
    );
};

export default Home;
