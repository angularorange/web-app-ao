import React, {useLayoutEffect, useState} from 'react';
import {Paper, MenuList, MenuItem, Stack} from "@material-ui/core";
import axios from "axios";
import {useLocation} from 'react-router-dom';

let getUrl = (location) => {
    const {category} = location.state;
    let categoryLowercase = category.toLowerCase().replace(/\s+/g, '-');
    console.log("categoryLowercase: " + categoryLowercase);
    let url = "https://aws-website-angularorange-wcrwx.s3.amazonaws.com/json/ao-course-" + categoryLowercase + ".json";
    return url;
}

const Course = () => {
    const location = useLocation()
    let myUrl = getUrl(location);
    const [courses, setCourses] = useState({});
    const [videos, setVideos] = useState([]);
    let videoCategory = courses.category
    let duration = courses.length;
    const [videoData, setVideoData] = useState({
        "videoUrl": "https://aws-website-angularorange-wcrwx.s3.amazonaws.com/courses/2023/intro-section1-ppt.mp4",
        "videoTitle": "Introduction"
    });

    useLayoutEffect(() => {
        axios.get(getUrl(location)).then((response) => {
            setCourses(response.data);
            setVideos(response.data.videos);
            setVideoData({"videoUrl": response.data.videos[0].videoUrl, "videoTitle": response.data.videos[0].title});
        })
    }, [myUrl, location]);


    return (
        <div key={myUrl}
             style={{
                 display: 'flex',
                 justifyContent: 'Right',
                 alignItems: 'Right',
                 height: '100vh'
             }}
        >
            <div>
                <span className="Video-category">{videoCategory}</span>
                <span className="Video-title">{videoData.videoTitle}</span>
                <Stack direction="row" spacing={2} justifyContent="space-evenly" alignItems="stretch">
                    <Paper>
                        <MenuList>
                            {videos.map(video => (
                                <MenuItem key={video.title} onClick={() => setVideoData({
                                    "videoUrl": video.videoUrl,
                                    "videoTitle": video.title
                                })}>{video.title}</MenuItem>
                            ))}
                        </MenuList>
                    </Paper>

                    <div className="Video">

                        <video src={videoData.videoUrl} width="750" height="500" controls>
                        </video>
                    </div>
                </Stack>
                <span className="Span-text">Duration: {duration}</span>
            </div>

        </div>
    );
};

export default Course;

